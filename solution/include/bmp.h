#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>
#include <stdint.h>


enum read_status {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER,
    READ_ALLOCATION_FAILURE,
    READ_BAD_IMAGE_DATA,
    READ_BAD_INPUT
};
enum read_status from_bmp(FILE *in, struct image *image);

enum write_status {
    WRITE_OK,
    WRITE_ERROR
};

enum file_status {
    OPEN_ERROR,
    CLOSE_ERROR,
    READ_ERROR,
    READ_GOOD,
    WRITE_GOOD
};

enum write_status to_bmp(FILE *out, struct image const *image);

enum file_status read_bmp_file(const char *file_name,struct image *image);

enum file_status write_bmp_file(const char *file_name, struct image *image);

#endif
