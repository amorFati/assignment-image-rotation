#include "image.h"
#include "bmp.h"
#include <inttypes.h>
#include <stdlib.h>

uint32_t BI_SIZE = 40;
uint16_t BF_TYPE = 0x4d42;
uint16_t BI_BIT_COUNT = 24;
uint16_t BI_PLANES = 1;
uint32_t HEADER_SIZE = 54;

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


static uint32_t padding(const uint32_t width) {
    if (width % 4 == 0) {
        return 0;
    }
    return 4 - ((width * sizeof(struct pixel)) % 4);
}


static uint32_t size_of_image(const uint32_t width, const uint32_t height, const uint32_t padding) {
    return (width * height * sizeof(struct pixel) + padding * height);
}

static struct bmp_header generate_bmp_header(const struct image *image) {
    uint32_t size = size_of_image(image->width, image->height, padding(image->width));
    return (struct bmp_header) {
            .bfType = BF_TYPE,
            .bfileSize = HEADER_SIZE + size,
            .bfReserved = 0,
            .bOffBits = HEADER_SIZE,
            .biSize = BI_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0

    };
}

enum write_status to_bmp(FILE *out, const struct image *image) {
    struct bmp_header header = generate_bmp_header(image);
    const uint8_t zero[] = {0, 0, 0, 0};
    const size_t pad = padding(image->width);

    if (image == NULL | out == NULL) {
        return WRITE_ERROR;
    }

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    if (fseek(out, header.bOffBits, SEEK_SET) != 0) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < image->height; ++i) {
        if (!fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, out) ||
            fwrite(&zero, pad, 1, out) != 1) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

enum read_status from_bmp(FILE *in, struct image *image) {
    struct bmp_header header = {0};
    if (image == NULL | in == NULL) {
        return READ_BAD_INPUT;
    }
    if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }
    *image = create_image(header.biWidth, header.biHeight);
    const size_t pad = padding(image->width);
    if (image->data == 0) {
        return READ_ALLOCATION_FAILURE;
    }

    for (size_t i = 0; i < image->height; ++i) {
        size_t res = fread(&image->data[image->width * i], sizeof(struct pixel), image->width, in);
        if (res != image->width) {
            delete_image(image);
            return READ_BAD_IMAGE_DATA;
        }
        if (fseek(in, pad, SEEK_CUR)) {
            return READ_BAD_IMAGE_DATA;
        }
    }

    return READ_OK;
}





