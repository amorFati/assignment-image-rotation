#include "image.h"
#include <stdlib.h>

struct image create_image(const uint32_t width, const uint32_t height) {
    struct image image = {0};
    image.data = malloc(sizeof(struct pixel) * width * height);
    if (image.data != NULL) {
        image.width = width;
        image.height = height;
    }
    return image;
}

struct image empty_image(struct image image) {
    image.data = NULL;
    image.width = 0;
    image.height = 0;
    return image;
}

struct image rotate(struct image const input) {
    struct image new_image = create_image(input.height, input.width);
    if (new_image.data == NULL) {
        return empty_image(new_image);
    }
    for (int32_t i = 0; i < input.height; i++) {
        for (int j = 0; j < input.width; j++) {
            new_image.data[j * input.height + (input.height - 1 - i)] = input.data[i * (input.width) + j];
        }
    }
    return new_image;
}

void delete_image(struct image  *img) {
    if (img == NULL) return;
    if(img->data == NULL) return;
    free(img->data);
    img->data = NULL;
}

