#include "image.h"
#include "bmp.h"
#include <stdio.h>

static void close_files(FILE *a, FILE *b) {
    if (!a) fclose(a);
    if (!b) fclose(b);
}

static void delete_images(struct image *a, struct image *b) {
    delete_image(a);
    delete_image(b);
}

int main(int argc, char *argv[]) {
    if(argc == 3) {
        FILE *in = fopen(argv[1], "rb");
        FILE *out = fopen(argv[2], "wb");
        struct image image;
        struct image rotated = {0};

        if (in == NULL || out == NULL) {
            close_files(in, out);
            fprintf(stderr, "Can't open file\n");
            return 1;
        }

        if (from_bmp(in, &image) != READ_OK) {
            close_files(in, out);
            delete_image(&image);
            fprintf(stderr, "Can't read bmp image\n");
            return 1;
        } else {
            rotated = rotate(image);
        }

        if (rotated.data == NULL) {
            delete_images(&image, &rotated);
            close_files(in, out);
            fprintf(stderr, "Can't rotate image\n");
            return 1;
        }

        if (to_bmp(out, &rotated) != WRITE_OK) {
            delete_images(&image, &rotated);
            close_files(in, out);
            fprintf(stderr, "Cannot write bmp\n");
            return 1;
        }

        delete_images(&image, &rotated);
        close_files(in, out);
        fprintf(stdout, "Image has rotated\n");
        return 0;
    }
}
